
//Il faut toujours ouvrir cette fonction
$(document).ready(function(){

  //on cible l'id submit : le click
  $('#submit').click( function(){

    //récupération des données
    let nom = $("#nom").val().trim() ;
    let prenom = $("#prenom").val().trim() ;
    let email = $("#email").val().trim() ;
    let message = $("#message").val().trim() ;

    //let chaine = $("form").serialize();
    let chaine = $("form").serializeArray();
    console.log(chaine);

    //vérifier que les champs ne sont pas vide
    if(nom == '' || prenom == ''){

      

      $('#nom').after('<span>Merci de remplir ce champs</span>');


      $('#prenom').after('<span>Merci de remplir ce champs</span>');
  

      preventDefault();
      //alert('remplir tous les champs');
      return false;//pour arrêter le script si c'est faux en javascript dans une fonction
    }

    // nom.length === 0{
    //   alert('remplir tous les champs');
    // };

    //méthode ajax : ne pas modifier les mots clés, ex: type
    $.ajax( {
      type: "POST",
      url: "submission.php",
      data: chaine //methode 2 avec serializeArray()
      //methode 1 avec recupération de toutes les données 1 par 1 {
      //   name : nom,
      //   firstName : prenom,
      //   email : email,
      //   message : message
      // }
      ,
      cache: false,
      success: function(reponse){
        //alert(reponse);
      },
      //fonction pour voir les erreurs
      error: function(xhr, status, error){
        console.error(xhr);
      }

    } );
//fonction callback    
// data.map( (elmt, innn)=>{
//   alert(elmt);
// } )



  } );


});